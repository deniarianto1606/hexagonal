package com.learn.hexagonal.app.entrypoint.controller;

import java.util.List;

import com.learn.hexagonal.core.person.PersonCore;
import com.learn.hexagonal.core.person.PersonCoreResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

    @Autowired
    private PersonCore personLogic;

    @GetMapping("/persons")
    public List<PersonCoreResponse> getPersons(){
        return personLogic.persons();
    }
    
}
