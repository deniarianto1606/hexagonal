package com.learn.hexagonal.provider.service;

import java.util.ArrayList;
import java.util.List;

import com.learn.hexagonal.provider.entity.Person;
import com.learn.hexagonal.provider.model.PersonServiceRequest;
import com.learn.hexagonal.provider.model.PersonServiceResponse;
import com.learn.hexagonal.provider.repository.PersonRepository;
import com.learn.hexagonal.core.person.PersonGateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements PersonGateway {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public List<PersonServiceResponse> getPerson() {
        List<Person> persons = personRepository.getPerson();
        List<PersonServiceResponse> personServiceResponses = new ArrayList<>();
        for(Person person : persons){
            String title = null;
            if(person.getLevel() == 1){
                title = "Junior";
            } else if(person.getLevel() == 2){
                title = "Middle";
            } else if(person.getLevel() == 3){
                title = "Senior";
            } else if(person.getLevel() == 4){
                title = "Lead";
            }

            
            PersonServiceResponse response = PersonServiceResponse.builder()
                .id(person.getId())
                .name(person.getName())
                .departmentId(person.getDepartmentId())
                .level(person.getLevel())
                .title(title)
                .build();
            
            personServiceResponses.add(response);
        }
        return personServiceResponses;
    }

    @Override
    public PersonServiceResponse save(PersonServiceRequest request) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
