package com.learn.hexagonal.provider.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.learn.hexagonal.provider.entity.Department;
import com.learn.hexagonal.provider.model.DepartmentServiceResponse;
import com.learn.hexagonal.provider.repository.DepartmentRepository;
import com.learn.hexagonal.core.department.DepartmentGateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImpl implements DepartmentGateway {

    @Autowired
    private DepartmentRepository departmentRepository;
    
    @Override
    public Map<String, DepartmentServiceResponse> getDepartments() {
        List<Department> departments = departmentRepository.getDepartments();
        Map<String, DepartmentServiceResponse> mapOfDeparments = new HashMap<>();
        for(Department department : departments){
            mapOfDeparments.put(department.getId(), DepartmentServiceResponse.builder()
                .id(department.getId())
                .name(department.getName())
                .build()
            );
        }

        return mapOfDeparments;
    }
    
}
