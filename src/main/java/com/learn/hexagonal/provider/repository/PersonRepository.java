package com.learn.hexagonal.provider.repository;

import java.util.ArrayList;
import java.util.List;

import com.learn.hexagonal.provider.entity.Person;

import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository {
    
    private List<Person> persons = new ArrayList<>();

    public PersonRepository(){
        Person deni = Person.builder()
            .id("1")
            .departmentId("1")
            .level(1)
            .name("Deni Arianto")
            .build();
        
        Person huda = Person.builder()
            .id("2")
            .departmentId("1")
            .level(4)
            .name("Huda Avianto")
            .build();

        Person harry = Person.builder()
            .id("3")
            .departmentId("2")
            .level(3)
            .name("Harry Gumbilar")
            .build();
        persons.add(deni);
        persons.add(huda);
        persons.add(harry);
    }

    public List<Person> getPerson(){
        return persons;
    }

    public Person save(Person person){
        persons.add(person);
        return person;
    }

}
