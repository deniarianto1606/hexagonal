package com.learn.hexagonal.provider.repository;

import java.util.ArrayList;
import java.util.List;

import com.learn.hexagonal.provider.entity.Department;

import org.springframework.stereotype.Repository;

@Repository
public class DepartmentRepository {
    private List<Department> departments = new ArrayList<>();

    public List<Department> getDepartments(){
        Department it = Department.builder().id("1").name("Technology").build();
        Department finance = Department.builder().id("2").name("Finance").build();
        Department marketing = Department.builder().id("3").name("Marketing").build();
        departments.add(it);
        departments.add(finance);
        departments.add(marketing);
        return departments;
    }
}
