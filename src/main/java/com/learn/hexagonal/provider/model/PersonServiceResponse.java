package com.learn.hexagonal.provider.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonServiceResponse {
    private String id;
    private String name;
    private int level;
    private String title;
    private String departmentId;
}
