package com.learn.hexagonal.core.person;

import java.util.List;

public interface PersonCore {
    public List<PersonCoreResponse> persons();
}
