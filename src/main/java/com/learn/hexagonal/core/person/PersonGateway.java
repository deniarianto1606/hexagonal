package com.learn.hexagonal.core.person;

import java.util.List;

import com.learn.hexagonal.provider.model.PersonServiceRequest;
import com.learn.hexagonal.provider.model.PersonServiceResponse;

public interface PersonGateway {
    public List<PersonServiceResponse> getPerson();
    public PersonServiceResponse save(PersonServiceRequest request);
}
