package com.learn.hexagonal.core.person;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.learn.hexagonal.provider.model.DepartmentServiceResponse;
import com.learn.hexagonal.provider.model.PersonServiceResponse;
import com.learn.hexagonal.core.department.DepartmentGateway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonCoreImpl implements PersonCore {

    @Autowired
    private PersonGateway personService;

    @Autowired
    private DepartmentGateway departmentService;

    @Override
    public List<PersonCoreResponse> persons() {
        List<PersonServiceResponse> persons = personService.getPerson();
        Map<String, DepartmentServiceResponse> mapOfDeparments = departmentService.getDepartments();
        List<PersonCoreResponse> personResponses = new ArrayList<>();
        for(PersonServiceResponse person : persons){
            personResponses.add(PersonCoreResponse.builder()
                .id(person.getId())
                .department(mapOfDeparments.get(person.getDepartmentId()).getName())
                .title(person.getTitle())
                .name(person.getName())
                .build()
            );
        }
        return personResponses;
    }
    
}
