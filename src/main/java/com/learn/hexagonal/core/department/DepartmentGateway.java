package com.learn.hexagonal.core.department;

import java.util.Map;

import com.learn.hexagonal.provider.model.DepartmentServiceResponse;

public interface DepartmentGateway {
    public Map<String, DepartmentServiceResponse> getDepartments();
}
